import uuid from 'uuid'

const newArray = [
    {quote: 'Любая достаточно развитая технология неотличима от магии.', author: 'someAuthor1', id:uuid()},
    {quote: 'Когда уважаемый, но пожилой учёный утверждает, что что-то возможно, то он почти наверняка прав. Когда он утверждает, что что-то невозможно, — он, весьма вероятно, ошибается.', author: 'someAuthor2', id:uuid()},
    {quote: 'Единственный способ обнаружения пределов возможного состоит в том, чтобы отважиться сделать шаг в невозможное.', author: 'someAuthor3', id:uuid()},
    {quote: 'Если президенты не могут делать этого со своими женами, они делают это со своими странами.', author: 'someAuthor4', id:uuid()},
    {quote: 'Большинство людей упускают появившуюся возможность, потому что она бывает одета в комбинезон и с виду напоминает работу.', author: 'someAuthor5', id:uuid()},
    {quote: 'Любое человеческое знание начинается с интуиции, переходит к понятиям и завершается идеями.', author: 'someAuthor6', id:uuid()},
    {quote: 'Хитрость - образ мыслей очень ограниченных людей и очень отличается от ума, на который по внешности походит.', author: 'someAuthor7', id:uuid()},
    {quote: 'Можно дурачить часть народа все время, можно дурачить весь народ некоторое время, но нельзя дурачить все время весь народ', author: 'someAuthor8', id:uuid()},
    {quote: 'Есть две бесконечные вещи — Вселенная и человеческая глупость. Впрочем, насчёт Вселенной я не уверен.', author: 'someAuthor9', id:uuid()},
    {quote: 'Представьте себе, какая была бы тишина, если бы люди говорили только то, что знают.', author: 'someAuthor10', id:uuid()},
    {quote: 'SomeQuote11', author: 'someAuthor11', id:uuid()}
]
export default newArray;