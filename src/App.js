import React from 'react';
import './App.css';
import newArray from './Array'
import Quote from './Components/Quote/Quote';
import { CSSTransition } from 'react-transition-group'
import QuotesList from './Components/QuotesList/QuotesList';
import uuid from 'uuid'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quotes: newArray,
      currentQuote: newArray[0].quote,
      showQuotesList: true
    }
    this.randomQuote = this.randomQuote.bind(this);
    this.toggleQuotesList = this.toggleQuotesList.bind(this);
    this.quoteDelete = this.quoteDelete.bind(this);
    this.quoteEdit = this.quoteEdit.bind(this);
    this.quoteAdd = this.quoteAdd.bind(this);
  }
  randomQuote() {

    const quotes = this.state.quotes.slice();
    if (quotes.length !== 0) {
      let currentQuote = quotes[Math.floor(Math.random() * quotes.length)].quote;
      this.setState({ currentQuote })
    }
    else this.setState({ currentQuote: 'There Nothing To Show :(' });
  }
  toggleQuotesList() {
    this.setState({
      showQuotesList: !this.state.showQuotesList
    })
  }
  quoteAdd(quote) {
    const newQuote = {
      id: uuid(),
      quote
    },
          newQuotes = [].concat(newQuote).concat(this.state.quotes);
    this.setState({
      quotes: newQuotes
    })

  }
  quoteDelete(index) {
    let newQuotes = this.state.quotes.slice();

    newQuotes.splice(index, 1);

    this.setState({
      quotes: newQuotes
    })
  }
  quoteEdit(value, index) {
    if (index) {
      const newQuotes = [].concat(this.state.quotes);

      newQuotes[index].quote = value;

      this.setState({
        quotes: newQuotes
      })
    }

  }

  render() {
    return (
      <div className='App'>
        <CSSTransition
          in={this.state.showQuotesList}
          timeout={500}
          classNames='app-node'
          unmountOnExit
        >
          <Quote
            quotes={this.state.quotes}
            currentQuote={this.state.currentQuote}
            randomQuote={this.randomQuote}
            toggleQuotesList={this.toggleQuotesList}
          />
        </CSSTransition>

        <CSSTransition
          in={!this.state.showQuotesList}
          timeout={500}
          classNames='app-node'
          unmountOnExit
        >
          <QuotesList
            toggleQuotesList={this.toggleQuotesList}
            quotes={this.state.quotes}
            quoteDelete={this.quoteDelete}
            quoteEdit={this.quoteEdit}
            quoteAdd={this.quoteAdd}
          />
        </CSSTransition>
      </div>);

  }

}

export default App;
