import 'glyphicons-only-bootstrap/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import './QuotesList.css';
import { TransitionGroup, CSSTransition } from 'react-transition-group';



class QuotesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            changedQuote: null,
            quoteIndex: null,
            addedQuote:'',
            quoteDeleted: false,
            quoteChanged: false,
            quoteAdded:false
        }
        this.quoteDelete = this.quoteDelete.bind(this);
        this.quoteEdit = this.quoteEdit.bind(this);
        this.quoteChange = this.quoteChange.bind(this);
        this.quoteInputChange = this.quoteInputChange.bind(this);
        this.quoteAdd = this.quoteAdd.bind(this);
    }
    quoteAdd(){
        if(this.state.addedQuote.length !== 0){
        this.setState({
            quoteAdded:true
        })
        setTimeout(() => this.setState({quoteAdded:false}), 1000);
        this.props.quoteAdd(this.state.addedQuote);
        this.setState({
            addedQuote: ''
        })
    }
       
    }
    quoteInputChange(e){
        const addedQuote = e.target.value;
        this.setState({addedQuote})
    }
    quoteDelete(e) {
        let index = e.target.dataset.index;
        console.log(e.target.dataset.index);
        this.setState({
            quoteDeleted: true
        })
        setTimeout(() => this.setState({ quoteDeleted: false }), 1000);
        this.props.quoteDelete(index);
    }
    quoteChange(e) {
        this.setState({
            changedQuote: e.target.value,
            quoteIndex: e.target.dataset.index
        })
    }
    quoteEdit(e) {
        let index = this.state.quoteIndex,
            value = this.state.changedQuote;

        if (this.state.changedQuote) {
            this.setState({ quoteChanged: true });

            setTimeout(() => this.setState({ quoteChanged: false, changedQuote: null }), 1000)

            this.props.quoteEdit(value, index);
        }
    }
    render() {
        return (
            <div className='quotes-list'>
                <h1 className='display-1 quotes-list__h1'>Quotes List</h1>
                <button className='btn btn-outline-dark' onClick={() => this.props.toggleQuotesList()}>Quote Machine</button>
                <div className='input-group add--quote'>
                    <input 
                    type="text" 
                    className="form-control" 
                    placeholder="Place Your Quote Here)"
                    value = {this.state.addedQuote}
                    onChange={this.quoteInputChange}
                     />
                    <div className="input-group-append">
                        <button 
                        className="btn btn-outline-secondary" 
                        type="button"
                        onClick={this.quoteAdd}
                        >Add Quote</button>

                    </div>
                </div>
                <TransitionGroup>
                    {this.props.quotes.map((item, index) => {
                        item.index = index;
                        return (
                            <CSSTransition
                                in={this.state.quoteAdded}
                                key={item.id}
                                timeout={500}
                                classNames='quote-editor-node'
                                unmountOnExit
                            >
                                <div className='form-group quote-edit quotes-list__wrapper' data-index={index} draggable={true} onDragEnd={this.quoteDelete}>
                                    <textarea
                                        className='form-control quotes-list__textarea'
                                        rows='4' data-index={index}
                                        onChange={this.quoteChange}
                                        defaultValue={item.quote}
                                    />
                                    <button
                                        className='btn btn-outline-success quotes-list__confirm-btn'
                                        data-index={index}
                                        onClick={this.quoteEdit}
                                    >
                                        <span className="glyphicon glyphicon-ok"></span>
                                    </button>
                                    <button
                                        className='btn btn-outline-danger quotes-list__remove-btn'
                                        data-index={index}
                                        onClick={this.quoteDelete}
                                    >
                                        <span className='glyphicon glyphicon-remove'></span>
                                    </button>
                                </div>
                            </CSSTransition>

                        )
                    })}
                </TransitionGroup>
                <CSSTransition
                    in={this.state.quoteDeleted || this.state.quoteChanged || this.state.quoteAdded}
                    timeout={300}
                    classNames='quote__status-node'
                    unmountOnExit
                >
                    <div className='quote__status'>
                        {this.state.quoteDeleted
                            &&
                            <h4 className='quote__status--message__deleted display-4'><span className='glyphicon glyphicon-remove'></span> Quote deleted</h4>
                        }
                        {this.state.quoteChanged
                            &&
                            <h4 className='quote__status--message__changed display-4'><span className="glyphicon glyphicon-ok"></span> Quote Changed</h4>
                        }
                        {this.state.quoteAdded
                            &&
                            <h4 className='quote__status--message__changed display-4'><span className="glyphicon glyphicon-plus"></span> Quote Added</h4>
                        }
                    </div>
                </CSSTransition>
            </div>)
    }
}

export default QuotesList;