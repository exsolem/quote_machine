import React from 'react'
import './Quote.css'
import { CSSTransition } from 'react-transition-group'

class Quote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showQuote: true
        }
        this.showQuote = this.showQuote.bind(this);
    }
    showQuote() {
        this.setState({
            showQuote: false
        })
        setTimeout(() => {
            this.setState({ showQuote: true })
            return this.props.randomQuote()
        }, 500)
    }
    render() {
        return (
            <div className='Quote'>
                <header className='Quote__header'>
                    <h1 className='display-1'>random quote machine</h1>
                    <button className='btn btn-outline-dark Quote__quotes-list-btn' onClick={() => this.props.toggleQuotesList(!this.props.bool)}>Quotes List</button>
                </header>
                <CSSTransition
                    in={this.state.showQuote}
                    timeout={500}
                    classNames='my-node'
                >

                    {this.props.currentQuote ? <h2 className='display-2 border'>{this.props.currentQuote}</h2> : <></>}

                </CSSTransition>
                <button className='btn btn-outline-success Quote__get-quote-btn' onClick={this.showQuote}>Get Quote</button>
            </div>
        )
    }
}
export default Quote;